// COPY TO CLIPBOARD
// Attempts to use .execCommand('copy') on a created text field
// Falls back to a selectable alert if not supported
// Attempts to display status in Bootstrap tooltip
// ------------------------------------------------------------------------------

function copyToClipboard(text, el) {
    var copyTest = document.queryCommandSupported('copy');
    var elOriginalText = el.attr('data-original-title');
  
    if (copyTest === true) {
      var copyTextArea = document.createElement("textarea");
      copyTextArea.value = text;
      document.body.appendChild(copyTextArea);
      copyTextArea.select();
      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'Copied!' : 'Whoops, not copied!';
        el.attr('data-original-title', msg).tooltip('show');
      } catch (err) {
        console.log('Oops, unable to copy');
      }
      document.body.removeChild(copyTextArea);
      el.attr('data-original-title', elOriginalText);
    } else {
      // Fallback if browser doesn't support .execCommand('copy')
      window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
    }
  }
  
  $(document).ready(function() {
    // Initialize
    // ---------------------------------------------------------------------
  
    // Tooltips
    // Requires Bootstrap 3 for functionality
    $('.js-tooltip').tooltip();
  
    // Copy to clipboard
    // Grab any text in the attribute 'data-copy' and pass it to the 
    // copy function
    $('.js-copy').click(function() {
      var text = $(this).attr('data-copy');
      var el = $(this);
      copyToClipboard(text, el);
    });
  });

  // function calcAndShowTotal() {
  //   var total = 0;
  //   $('#catlist :checkbox:checked').each(function() {
  //     total += parseFloat($(this).attr('price')) || 0;
  //   });
  //   $('#total').val(total);
  // }
  
  // $('#catlist :checkbox').change(calcAndShowTotal).change();

  function totalIt() {
    var input = document.getElementsByName("product");
    var total = 0;
    for (var i = 0; i < input.length; i++) {
      if(input[i].tagName == 'SELECT'){
        total += Number(input[i].options[input[i].selectedIndex].value);
      }
      if (input[i].checked) {
        total += parseFloat(input[i].value);
      }
    }
    document.getElementById("total").value = "R" + total.toFixed(2);
  }
  if(input[i].tagName == 'SELECT'){
    total += Number(input[i].options[input[i].selectedIndex].value);
  }

  function ShowHideDiv() {
    var chkYes = document.getElementById("chkYes");
    var dvtext = document.getElementById("dvtext");
    dvtext.style.display = chkYes.checked ? "block" : "none";
}

function showDiv(divId, element)
{
    document.getElementById(divId).style.display = element.value == 3 ? 'block' : 'none';
}
