Website prices
--------------
- Domain (subjective)
- Hosting (packaged)
- Site design subjective (+1700 per package)
- Logo design (R600)

Basic:
R150 pm hosting = 900
R2100 Design cost
6x Months of hosting + design
= R3000 (expense = 540)
Profit: R2460

Premium:
R200 pm hosting = 1200
R3800 Desgin cost
= R5000 (expense = 840)
Profit: R4160


Developer:
R250 pm hosting = 1500
R5500 Design cost
= R8000 (expense = 1200)
Profit: R6800






See more buttons:
Basic:

    5 GB Storage
    Custom domain
    Email/Discord Support
    Fully customizeable
    Basic Website / Landing page
    Free SSL certificate
    25 Email accounts

    The basic package includes a basic website/landing page, with 5 pages or less.

    No advanced functionality like user account management, forums, or databases are included, but the basics like contact forms, maps, and information etc.

Premium:

    10 GB Storage
    Custom domain
    Email/Discord Support
    Fully customizeable
    Responsive website
    Free SSL certificate
    100 Email accounts

    The premium package includes a fully functional and responsive website, with 15 pages or less.

    Advanced Design elements like animations and moving items are optional.

    This website can be customized at any time with support from our team.


Developer: 

    20 GB Storage
    Custom domain
    Email/Discord Support
    Fully customizeable
    Advanced responsive website
    Free SSL certificate
    Data handling + user accounts
    Advanced functionality
    200 Email accounts

    The developer package includes a highly advanced website with almost any functionality (Forums, User Accounts, Search Functionality, and other scripts)

    Premium design elements will be used (Anything from button animations to moving backgrounds)

    This website is customizeable down to every single detail, and will recieve priority support from 0xside.


